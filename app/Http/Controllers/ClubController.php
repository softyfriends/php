<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\Club;

class ClubController extends Controller
{
	public function index()
		{
			$clubs = Club::all();

			return view('clubs.index')->with('clubs', $clubs);
		}
	
	public function create()
		{
			return view('clubs.create');
		}

	public function store(Request $request)
		{
		/*	There are two ways to enter products into a table as below

			$product = new Product();

			$product->name = $request->name;
			$product->price = $request->price;
			$product->save();

			return "Something"; */

			$inputs = $request->all();
			$club = Club::Create($inputs);

			//return redirect()->route('Product.index');
		
			// THE ABOVE REDIRECT METHOD WE CAN ALSO WRITE AS BELOW.

			return redirect()->action('ClubController@index');
		
		}

		public function show($id)
			{
			
				$club = Club::find($id);

				// return $product;  here it prints in json code but we have to print in web page then 

				return view('clubs.show')->with('club', $club);
			}

		  public function destroy($id)
			{
			
				//Product::destroy($id);

				$club = Club::find($id)
								->delete();

				return redirect()->route('Club.index');
			} 

		public function edit($id)
			{
			
				$club = Club::find($id);

				// return view('products.edit')->with('product', $product);  OR

				return view('clubs.edit', compact('club', $club));
			}

		public function update(Request $request, $id)
			{
			
				$club = Club::find($id);

				$club->name = $request->name;
				$club->description = $request->description;

				$club->save();

				return redirect()->route('Club.index');
			}
}
