<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    protected $fillable = ['name', 'description'];

	public $timestamps = false;

	protected $table = 'clubs';
}
