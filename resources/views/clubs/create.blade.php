@extends('layouts.layoutclub')

@section('title')

	Create new Product

@stop

@section('body')

{!! Form::open(['route'=>'Club.store']) !!}

{!! Form::label('name', ' Club Name') !!}

{!! Form::text('name', null, ['placeholder'=> "Give club name"]) !!}

</br>

{!! Form::label('description', 'Description') !!}

{!! Form::text('description', null, ['placeholder'=> "Give description"]) !!}

{!! Form::submit('Create') !!}

{!! Form::close() !!}

@stop