@extends('layouts.layoutclub')

@section('title')
	
	All clubs

@stop

@section('body')

@foreach($clubs as $club)
	
	<h1> {{ $club->name}}</h1>
	<h3> {{ $club->description}}</h3>
	</br>

@endforeach

@stop