@extends('layouts.layoutclub')

@section('title')

	{{ $club->name }}

@stop

@section('body')

{!! Form::open([

		'method'=>'delete',
		'route'=>['Club.destroy', $club->id]
		
		]) !!}

<h1> {{ $club->name }} </h1>

<h3> {{ $club->description }} </h3>

<a href="{{route('Club.edit', $club->id)}}"> Edit </a> 

{!! Form::submit('Delete') !!}

{!! Form::close() !!}

@stop