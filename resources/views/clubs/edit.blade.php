@extends('layouts.layoutclub')

@section('title')

 Edit	{{ $club->name }}

@stop

@section('body')

{!! Form::model( $club, [

		'method'=>'patch',
		'route'=>['Club.update', $club->id]
		
		]) !!}

{!! Form::label('name', 'Name') !!}
{!! Form::text('name', $club->name, ['placeholder'=>"Give a name"]) !!}

</br>

{!! Form::label('description', 'Description') !!}
{!! Form::text('description', $club->description, ['placeholder'=>"Give a description"]) !!}


{!! Form::submit('Edit') !!}

{!! Form::close() !!}

@stop