<!DOCTYPE html>

<html>
<head> 
			<title>@yield('title')</title>
</head>

<body>
	
	@include('layouts.menuclub')
	
	@yield('body')

	{{-- This comment will not be in the rendered HTML --}}

</body>

</html>